交易系统
========

.. py:module:: hikyuu.trade_sys
    

.. toctree::

   environment
   condition
   signal
   stoploss
   money_manager
   profitgoal
   slippage
